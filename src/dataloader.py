from keras.datasets import mnist
import numpy as np


class MnistBags():

    def __init__(self, target_number, mean_bag_length=10, var_bag_length=2, num_bag=250, seed=1, train=True):
        self.target_number = target_number
        self.mean_bag_length = mean_bag_length
        self.var_bag_length = var_bag_length
        self.num_bag = num_bag
        self.train = train

        self.r = np.random.RandomState(seed)

        self.num_in_train = 60000
        self.num_in_test = 10000

        if self.train:
            self.train_bags_list, self.train_labels_list = self._create_bags()
        else:
            self.test_bags_list, self.test_labels_list = self._create_bags()

    def _create_bags(self):
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        if self.train:
            x, y = x_train, y_train
        else:
            x, y = x_test, y_test

        # add channel dimension
        x = x[..., np.newaxis]
        # normalize
        x = x / 255.

        bags_list = []
        labels_list = []

        for i in range(self.num_bag):
            bag_length = np.int(self.r.normal(self.mean_bag_length, self.var_bag_length, 1))
            if bag_length < 1:
                bag_length = 1

            if self.train:
                indices = np.random.choice(range(self.num_in_train), size=bag_length, replace=False)
            else:
                indices = np.random.choice(range(self.num_in_test), size=bag_length, replace=False)

            # boolean mask for ground truth
            labels_in_bag = y[indices]
            labels_in_bag = labels_in_bag == self.target_number

            bags_list.append(x[indices])
            labels_list.append(labels_in_bag)

        return bags_list, labels_list

    def __len__(self):
        if self.train:
            return len(self.train_labels_list)
        else:
            return len(self.test_labels_list)

    def __getitem__(self, index):
        if self.train:
            bag = self.train_bags_list[index]
            label = [max(self.train_labels_list[index]), self.train_labels_list[index]]
        else:
            bag = self.test_bags_list[index]
            label = [max(self.test_labels_list[index]), self.test_labels_list[index]]

        return bag, label