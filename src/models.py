import keras
from keras.models import Model
from keras.layers import Input, Conv2D, Activation, Dense, MaxPooling2D, Flatten, Softmax, dot, Lambda
import keras.backend as K
import numpy as np
from dataloader import MnistBags

class NoCheckModel(Model):
    '''avoid check for equal sized inputs and targets: https://github.com/keras-team/keras/pull/11548'''
    def _standardize_user_data(self, *args, **kwargs):
        kwargs['check_array_lengths'] = False
        return super()._standardize_user_data(*args, **kwargs)

class AttentionMIL():

    def __init__(self, target_number, gating=False, bag_size_mean=10, bag_size_var=2):
        self.L = 500
        self.D = 128
        self.K = 1

        self.h = 28
        self.w = 28
        self.c = 1

        self.target_number = target_number
        self.gating = gating

        self.bag_size_mean = bag_size_mean
        self.bag_size_var  = bag_size_var

        self.train_loader = None
        self.test_loader = None
        self.build_model()
        self.compile_model()


    def build_model(self):

        # function to make Lambda layers for transpose
        def transpose(x): return K.transpose(x)
        # function to make Lambda layers for dot product
        def dot(x): return K.dot(x[0], x[1])

        i = Input((self.h, self.w, self.c))

        # feature extractor part 1
        x = Conv2D(filters=20, kernel_size=(5, 5))(i)
        x = Activation('relu')(x)
        x = MaxPooling2D(pool_size=(2, 2), strides=2)(x)
        x = Conv2D(filters=50, kernel_size=(5, 5))(x)
        x = Activation('relu')(x)
        x = MaxPooling2D(pool_size=(2, 2), strides=2)(x)

        # feature extractor part 2
        H = Flatten()(x)
        H = Dense(self.L)(H)
        H = Activation('relu')(H)  # NxL

        print(f'K.int_shape(H) = {K.int_shape(H)} # NxL')

        # attention
        A = Dense(self.D)(H)
        At = Activation('tanh')(A)
        At = Dense(self.K)(At)  # NxK

        # gating mechanism in attention (a la Dauphin 2016) to include learnable non-linearity (eq. 9)
        if self.gating:
            from keras.layers import Multiply
            As = Activation('sigmoid')(A) # NxK
            As = Dense(self.K)(As)  # NxK
            print(f'K.int_shape(At) = {K.int_shape(At)} # KxN')
            print(f'K.int_shape(As) = {K.int_shape(As)} # KxN')

            A = Multiply()([At, As])
        else:
            A = At
        A = Lambda(transpose)(A)
        A = Softmax(axis=1, name='attention')(A)  # KxN, eq. 8 (or eq. 9 of gating=True)

        print(f'K.int_shape(A) = {K.int_shape(A)} # KxN')

        # MIL pooling: multiply output with attention (eq. 7)
        x = Lambda(dot)([A,H])

        print(f'K.int_shape(x) = {K.int_shape(x)} # KxL')

        # classifier
        x = Dense(1)(x)
        Y_prob = Activation('sigmoid', name='probabilities')(x)
        print(f'K.int_shape(Y_prob) = {K.int_shape(Y_prob)} # 1')

        self.model = NoCheckModel(inputs=i, outputs=[Y_prob])

        self.model.summary()

    def compile_model(self):
        self.model.compile(optimizer='adam', loss=self.loss, metrics=['acc'])

    def forward(self, i, training=False):
        print(i.shape)

        outputs = [self.model.get_layer('probabilities').output, self.model.get_layer('attention').output]
        #print(outputs.name)
        functors = [K.function([self.model.input, K.learning_phase()], [out]) for out in outputs]
        #y_prob, attention = self.model.predict(i)
        y_prob, attention = [func([i, training]) for func in functors]
        y_prob = np.squeeze(y_prob)
        y_hat = K.eval(K.greater_equal(y_prob, 0.5))

        return y_prob, y_hat, attention

    def loss(self, y_true, y_pred):
        return keras.losses.binary_crossentropy(y_true, y_pred)

    def train(self, epochs):

        if self.train_loader is None:
            self.get_data(self.target_number)

        for i in range(1,epochs+1):
            train_loss = 0.
            train_acc = 0.
            for batch_idx, (x, y) in enumerate(self.train_loader):
                y = y[0]
                y = y[...,np.newaxis]
                loss, acc = self.model.train_on_batch(x, y)
                print('.', end='')
                train_loss += loss
                train_acc  += acc
            print(f'\nepoch: {i}\tloss: {train_loss/len(self.train_loader)}\taccuracy: {train_acc/len(self.train_loader)}')

    def get_data(self, target_number):
        self.train_loader = MnistBags(target_number=self.target_number, mean_bag_length=self.bag_size_mean, var_bag_length=self.bag_size_var, train=True)
        self.test_loader  = MnistBags(target_number=self.target_number, mean_bag_length=self.bag_size_mean, var_bag_length=self.bag_size_var, train=False)

